import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.Date;

public class SimpleHTTPServer {

  public static void main(String[] args) throws IOException {
    ServerSocket server = new ServerSocket(8080);

    System.out.println("Listening for connection on port 8080...");

    while (true) {
      try (Socket clientSocket = server.accept()) {
        // NOTE(antonio): Reading and outputting request to console
        System.out.println("Reading request...");

        InputStreamReader inputStreamReader =
          new InputStreamReader(clientSocket.getInputStream());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String currentLine = bufferedReader.readLine();
        while (!currentLine.isEmpty()) {
          System.out.println(currentLine);

          currentLine = bufferedReader.readLine();
        }
        // --------------------------------------------------------------------

        // NOTE(antonio): Sending today's date
        Date today = new Date();
        String httpResponse = "HTTP/1.1 200 OK\r\n\r\n" + today;

        clientSocket.getOutputStream().write(httpResponse.getBytes("UTF-8"));
      }
    }
  }
}
