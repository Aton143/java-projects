import java.util.Scanner;

class HelloWorld
{
  public static void main(String[] args)
  {
    /*
       Scanner here is a bit embarassing

       Scanner Input = new Scanner(System.in);

       System.out.print("Please enter an integer: ");
       int InputInt = Input.nextInt();
       System.out.println("The user inputted " + InputInt);

       Input.close();
    */

    int AgeOne = 12;
    int AgeTwo = 13;
    float AgeFloat = 12.0f;
    double AgeDouble = 12.121212;
    boolean IsOld = (AgeOne < AgeFloat);
    char Character = 'a';
    String AgeString = "I am 12 years old";

    System.out.println(AgeOne);
    System.out.println(AgeFloat);
    System.out.println(AgeDouble);
    System.out.println(IsOld);
    System.out.println(Character);
    System.out.println(AgeString);
    System.out.println("Age 1 + Age 2 = " + (AgeOne + AgeTwo));
    System.out.println("Age 1 * Age 2 = " + (AgeOne * AgeTwo));
    System.out.println("Age 2 / Age 1 = " +  ((float) AgeTwo / (float) AgeOne));
    System.out.println("AgeString is an instance of String? " + (AgeString instanceof String));

    int[] AgeArray = {10, 12, 19, 21};
    int AgeSum = 0;

    System.out.println("Age Array");
    for (int Age: AgeArray)
    {
      AgeSum += Age;
      System.out.println(Age);
    }
    System.out.println(AgeSum);
  }
}
